"use strict";
const app = require("express")(),
    bodyParser = require("http-body-parser").express,
    compression = require("compression"),
    cookieParser = require("cookie-parser"),
    cors = require("cors"),
    express = require("express"),
    initdb = require("./initdb.js"),
    jwt = require("jsonwebtoken"),
    port = 3000,
    sessionLengthInSeconds = 43200000,
    sessionTokenSecret =
    "SESSIONTOKENSECRETFORINVENTORY23927312327LOL#1232893274",
    sha512 = require("hash.js/lib/hash/sha/512"),
    sqlite3 = require("sqlite3").verbose(),
    minView = require("express-min-view"),
    electron = require("electron");

global.db = new sqlite3.Database("./db/inventory.db");
global.jwt = jwt;
global.sessionTokenSecret = sessionTokenSecret;
global.hashPass = function hashPass (pass) {

    const PASSSALT = "JUUUUUUUUUUUUUU,boy,434934,LOL";
    return sha512().update(PASSSALT + pass).
        digest("hex");

};

global.sessionExpTime = function sessionExpTime () {

    const date = new Date();
    return Math.floor(date.getTime() + sessionLengthInSeconds);

};

global.genToken = function genToken (email) {

    return jwt.sign(
        {
            "data": email,
            "exp": global.sessionExpTime()
        },
        sessionTokenSecret
    );

};

app.use(minView());
app.use(compression());
app.use(express.static("html"));
app.use(bodyParser({}));
app.use(cookieParser());
app.use(cors());

initdb();
app.listen(port);

app.get(
    "/api/getBillCount",
    require("./requests/getBillCount.js")
);
app.get(
    "/api/increaseBillCount",
    require("./requests/increaseBillCount.js")
);
app.get(
    "/api/instantiateInventory/:confirmToken",
    require("./requests/instantiateInventory.js")
);
app.get(
    "/api/setLabel/:index/:newLabel",
    require("./requests/setLabel.js")
);
app.get(
    "/api/isEmployee",
    require("./requests/isEmployee.js")
);
app.get(
    "/api/setPrefill/:data",
    require("./requests/setPrefill.js")
);
app.get(
    "/api/getPrefill",
    require("./requests/getPrefill.js")
);
app.get(
    "/api/register/:email/:password",
    require("./requests/register.js")
);
app.get(
    "/api/editItem/:id/:newValues",
    require("./requests/editItem.js")
);
app.get(
    "/api/decreaseItemQuantity/:itemIds/:itemQuantities",
    require("./requests/decreaseItemQuantity.js")
);
app.get(
    "/api/addItem/:newValues/:updateInventory",
    require("./requests/addItem.js")
);
app.get(
    "/api/removeItem/:itemId",
    require("./requests/itemId.js")
);
app.get(
    "/api/showInventory",
    require("./requests/showInventory.js")
);
app.get(
    "/api/getItems",
    require("./requests/getItems.js")
);
app.get(
    "/api/getLabels",
    require("./requests/getLabels.js")
);

app.get(
    "/api/removeEmployee/:employeeEmail",
    require("./requests/employeeEmail.js")
);

app.get(
    "/api/getEmployees",
    require("./requests/getEmployees.js")
);

app.get(
    "/api/addEmployee/:employeeEmail/:employeePassword",
    require("./requests/addEmployee.js")
);


app.get(
    "/api/login/:email/:password",
    require("./requests/login.js")
);
app.get(
    "/api/logout",
    require("./requests/logout.js")
);

electron.app.on("ready", function() {
    let window = new electron.BrowserWindow({
        
    })
    window.loadURL(`file:///${__dirname}/html/index.html`);
})
