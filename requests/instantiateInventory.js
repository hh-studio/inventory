"use strict";

/*
 * DEF:
 *  Imports sheet data into the database
 *
 * ARGS:
 *      Secret token to make sure this thing isnt run accidentally
 *
 *  EXMAMPLE:
 *  /api/instantiateInventory/hjuu555
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({"com": "",
                    "data": {"error": "Et ole kirjautunut sisään"}}));

            } else {

                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE  employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        if (req.params.confirmToken !== "hjuu555") {

                            res.send(JSON.stringify({
                                "com": "instantiateInventory",
                                "data": {
                                    "error":
                                    `Salainen avain väärin, tämä tarkistus on
                                        tehty vahinkojen välttämiseksi`
                                }
                            }));
                            return;

                        }

                        if (encoded.data !== ownerEmail) {

                            res.send(JSON.stringify({
                                "com": "instantiateInventory",
                                "data": {
                                    "error": `Vain varaston omistaja
                                        voi alustaa varaston`
                                }
                            }));
                            return;

                        }

                        global.db.run(
                            `DELETE FROM items
                             WHERE owner='${ownerEmail}'`,
                            (instantiateInventoryErr) => {

                                if (instantiateInventoryErr) {

                                    console.log(`instantiateInventory ERROR 1:
                                        ${instantiateInventoryErr}`);
                                    res.send(JSON.stringify({
                                        "com": "instantiateInventory",
                                        "data": {
                                            "error": `Taulukon tuonti 
                                                epäonnistui (Ei voitu tyhjentää
                                                varastoa uusien tietojen 
                                                tieltä)`
                                        }
                                    }));

                                } else {

                                    global.db.run(
                                        `UPDATE labels
                                         SET
                                            field1='none',
                                            field2='none',
                                            field3='none',
                                            field4='none',
                                            field5='none',
                                            field6='none',
                                            field7='none',
                                            field8='none',
                                            field9='none',
                                            field10='none',
                                            field11='none',
                                            field12='none',
                                            field13='none',
                                            field14='none',
                                            field15='none',
                                            field16='none',
                                            field17='none',
                                            field18='none',
                                            field19='none',
                                            field20='none',
                                            field21='none',
                                            field22='none',
                                            field23='none',
                                            field24='none'
                                         WHERE owner='${ownerEmail}'`,
                                        (labelsErr) => {

                                            if (labelsErr) {

                                                console.log(
                                                    `instantiateInventory
                                                    ERROR 2: ${labelsErr}`);
                                                res.send(JSON.stringify({
                                                    "com":
                                                        "instantiateInventory",
                                                    "data": {
                                                        "error": `Taulukon 
                                                            tuonti epäonnistui 
                                                            (Ei voitu tyhjentää 
                                                            varaston otsikoita 
                                                            uusien tietojen 
                                                            tieltä)`
                                                    }
                                                }));

                                            } else {

                                                res.send(JSON.stringify({
                                                    "com":
                                                        "instantiateInventory",
                                                    "data": {
                                                        "command": `Varasto 
                                                            alustettu 
                                                            onnistuneesti`
                                                    }
                                                }));

                                            }

                                        }
                                    );

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};

