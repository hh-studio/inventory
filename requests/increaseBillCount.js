"use strict";

/*
 * DEF:
 *      Increments count of bills of users or users employees
 *
 * ARGS:
 *
 *  EXMAMPLE:
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                global.db.get(
                    `SELECT  employer
                     FROM    employees
                     WHERE   employee='${encoded.data}'`,
                    (databaseErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        global.db.run(
                            `UPDATE users
                             SET    billCount = billCount + 1
                             WHERE  email='${ownerEmail}'`,
                            (increaseBillCountErr) => {

                                if (increaseBillCountErr) {

                                    res.send(JSON.stringify({
                                        "com": "increaseBillCount",
                                        "data": {
                                            "error":
                                                `Laskunumeron 
                                                    muutos epäonnistui`
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "increaseBillCount",
                                        "data": {
                                            "ignoreMSG": `Laskunumeron 
                                                muutos onnistui`
                                        }
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};
