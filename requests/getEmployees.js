"use strict";

/*
 * DEF:
 *      Gets all users employees from database
 *
 * ARGS:
 *      none
 *
 * EXAMPLE:
 *      Just make request when logged in, users email is taken from cookie
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (err, encoded) => {

            if (err) {

                res.send(JSON.stringify({
                    "com": "getEmployees",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                global.db.all(
                    `SELECT id, employee as email
                     FROM employees
                     WHERE employer='${encoded.data}'`,
                    (employeeErr, rows) => {

                        res.send(JSON.stringify({
                            "com": "getEmployees",
                            "data": rows
                        }));

                    }
                );

            }

        }
    );

};
