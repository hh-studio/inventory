"use strict";

/*
 * DEF:
 *      Gets users labels from database and returns them users labels
 *
 * ARGS:
 *      none
 *
 * EXAMPLE:
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "getLabels",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let labelEmail = encoded.data;
                        if (employerRow) {

                            labelEmail = employerRow.employer;

                        }
                        global.db.get(
                            `SELECT *
                             FROM labels
                             WHERE owner='${labelEmail}';`,
                            (getLabelsErr, row) => {

                                const labels = [];
                                Object.keys(row).forEach((key, index) => {

                                    if (index > 0 && row[key] !== "none") {

                                        labels.push(row[key]);

                                    }

                                });
                                res.send(JSON.stringify({
                                    "com": "getLabels",
                                    "data": {
                                        labels,
                                        "owner": labelEmail
                                    }
                                }));

                            }
                        );

                    }
                );

            }

        }
    );

};
