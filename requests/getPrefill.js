"use strict";

/*
 * DEF:
 *  Gets prefill data from user or their employer
 *
 * ARGS:
 *
 *  EXMAMPLE:
 *  /api/getPrefill
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE   employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        global.db.get(
                            `SELECT *
                            FROM prefill
                            WHERE owner ='${ownerEmail}'`,
                            (err, row) => {

                                if (err) {

                                    console.log(`prefill ERROR: ${err}`);
                                    res.send(JSON.stringify({
                                        "com": "getPrefill",
                                        "data": {
                                            "log": "Error getting prefill data"
                                        }
                                    }));

                                } else {

                                    const data = {};
                                    Object.keys(row).forEach((key) => {

                                        data[key] = row[key];

                                    });
                                    res.send(JSON.stringify({
                                        "com": "getPrefill",
                                        data
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};
