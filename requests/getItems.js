"use strict";

/*
 * DEF:
 *      Gets all user(or emloyer) owned items from database
 *
 * ARGS:
 *      none
 *
 * EXAMPLE:
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "getItems",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let itemsEmail = encoded.data;
                        if (employerRow) {

                            itemsEmail = employerRow.employer;

                        } else {

                            itemsEmail = encoded.data;

                        }

                        global.db.all(
                            `SELECT *
                             FROM items
                             WHERE owner='${itemsEmail}';`,
                            (getItemsErr, rows) => {

                                const items = [];
                                rows.forEach((row) => {

                                    const item = [];
                                    Object.keys(row).forEach((key, index) => {

                                        if (index > 0) {

                                            item.push(row[key]);

                                        }

                                    });
                                    items.push(item);

                                });
                                res.send(JSON.stringify({
                                    "com": "getItems",
                                    "data": {
                                        items,
                                        "owner": itemsEmail
                                    }
                                }));

                            }
                        );

                    }
                );

            }

        }
    );


};
