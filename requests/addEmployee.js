"duse strict";

/*
 * DEF:
 *      Adds employee to employer
 * ARGS:
 *      employeeEmail: Email of the employee being added
 *      employeePassword: Password of the employee being added
 *
 * EXAMPLE:
 *      employeeEmail: devmail1@mail.com
 *      We get the employer email from cookie, then add devmail1@mail.com user into users table
 *      and then employer-employee relation into employees table
 */
const register = require("./register.js");
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "addEmployee",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                function add () {

                    global.db.serialize(() => {

                        global.db.run(
                            `INSERT INTO employees (
                                employer,
                                employee
                             )
                             VALUES (
                                '${encoded.data}',
                                '${req.params.employeeEmail}'
                             );`,
                            (addEmployeeErr) => {

                                if (addEmployeeErr) {

                                    console.log(`addEmployee error:
                                        ${addEmployeeErr}`);
                                    res.send(JSON.stringify({
                                        "com": "addEmployee",
                                        "data": {
                                            "error": `Työntekijää ei voitu
                                                lisätä, todennäköisesti
                                                jo työntekijänä`
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "addEmployee",
                                        "data": "Työntekijä lisätty!"
                                    }));

                                }

                            }
                        );

                    });

                }
                // For registering employee
                req.params.email = req.params.employeeEmail;
                req.params.password = req.params.employeePassword;
                register(req, {"send" (text) {

                    add();

                }});

            }

        }
    );

};
