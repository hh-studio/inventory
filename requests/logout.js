"use strict";

/*
 * DEF:
 *      Logs user out by deleting the cookie
 *
 * ARGS:
 *      none
 *
 * EXAMPLE:
 *      Just call this while logged in and your cookie is removed
 */
module.exports = (req, res) => {

    res.send(JSON.stringify({
        "com": "logout",
        "data": "Uloskirjautuminen onnistui"
    }));

};
