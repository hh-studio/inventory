"use strict";

/*
 * DEF:
 *  Saves employers prefill data to be used on employees
 *
 * ARGS:
 *  Data:
 *      Prefill data to be saved
 *
 *  EXMAMPLE:
 *  /api/setPrefill/:data
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE  employee= '${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        if (encoded.data !== ownerEmail) {

                            res.send(JSON.stringify({"com": "setPrefill",
                                "data": {
                                    "error":
                                        `Vain varaston omistaja voi 
                                        asettaa esitäytön`
                                }}));
                            return;

                        }
                        const cartBillInfoData = (data) => JSON.
                                stringify(data.cartBillInfoData),
                            prefillData = JSON.parse(req.params.data),
                            seller = (data) => JSON.
                                stringify(data.seller);

                        global.db.get(
                            `UPDATE prefill
                             SET seller =
                                '${seller(prefillData)}',
                                cartBillInfoData =
                                '${cartBillInfoData(prefillData)}'
                             WHERE owner ='${ownerEmail}'`,
                            (setPrefillErr) => {

                                if (setPrefillErr) {

                                    console.log(`prefill ERROR:
                                        ${setPrefillErr}`);
                                    res.send(JSON.stringify({
                                        "com": "setPrefill",
                                        "data": {
                                            "log": "Error getting prefill data"
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "setPrefill",
                                        "data": {
                                            "msg": "Success"
                                        }
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};
