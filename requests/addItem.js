"use strict";

/*
 * DEF:
 *      Adds item into database
 *
 * ARGS:
 *      newValues: list of new items values
 *      updateInventory: Add boolean to response that makes
 *          frontend refresh inventory
 *
 * EXAMPLE:
 *      newValues: ["devmail@mail.com", 10, "bc", 0, 12]
 *      This would add new item to database with
 *          owner: devmail@mail.com
 *          quantity: 10
 *          barcode: bc
 *          field1: 0
 *          field2: 12
 *      and the rest will be filled with string "none"
 *
 */

const maxValues = 27;
let newValues = "";

module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            let updateInventory = true;
            if (req.params.updateInventory) {

                updateInventory = false;

            }
            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "addItem",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        // Fill values not given as parameter with string "none"
                        try {

                            newValues = JSON.parse(req.params.newValues);

                        } catch (JSONParseErr) {

                            console.log(`addItem ERROR: JSON.parse failed with
                                value: ${req.params.newValues}`);
                            res.send(JSON.stringify({
                                "com": "addItem",
                                "data": {
                                    "ignoreerror": `addItem ERROR: JSON.parse
                                        failed with value:
                                        ${req.params.newValues}`
                                }
                            }));
                            return;

                        }
                        while (newValues.length < maxValues) {

                            newValues.push("none");

                        }

                        newValues.forEach((item, index) => {

                            if (!item ||
                                item === "undefined" ||
                                item === null) {

                                newValues[index] = "";

                            }

                        });
                        global.db.run(`INSERT INTO items (
                                  owner,
                                  quantity,
                                  price,
                                  barcode,
                                  field1,
                                  field2,
                                  field3,
                                  field4,
                                  field5,
                                  field6,
                                  field7,
                                  field8,
                                  field9,
                                  field10,
                                  field11,
                                  field12,
                                  field13,
                                  field14,
                                  field15,
                                  field16,
                                  field17,
                                  field18,
                                  field19,
                                  field20,
                                  field21,
                                  field22,
                                  field23,
                                  field24,
                                  field25
                             )
                             VALUES (
                                '${ownerEmail}',
                                '${newValues[0]}',
                                '${newValues[1]}',
                                '${newValues[2]}',
                                '${newValues[3]}',
                                '${newValues[4]}',
                                '${newValues[5]}',
                                '${newValues[6]}',
                                '${newValues[7]}',
                                '${newValues[8]}',
                                '${newValues[9]}',
                                '${newValues[10]}',
                                '${newValues[11]}',
                                '${newValues[11]}',
                                '${newValues[12]}',
                                '${newValues[13]}',
                                '${newValues[14]}',
                                '${newValues[15]}',
                                '${newValues[16]}',
                                '${newValues[17]}',
                                '${newValues[18]}',
                                '${newValues[19]}',
                                '${newValues[20]}',
                                '${newValues[21]}',
                                '${newValues[22]}',
                                '${newValues[23]}',
                                '${newValues[24]}',
                                '${newValues[25]}',
                                '${newValues[26]}'
                             )`, (addItemErr) => {

                            if (addItemErr) {

                                console.log(`addItem ERROR: ${addItemErr}`);
                                res.send(JSON.stringify({
                                    "com": "addItem",
                                    "data": {
                                        "error": `Tuotteen lisäys
                                                epäonnistui, ole hyvä ja yritä
                                                kohta uudelleen`
                                    }
                                }));

                            } else {

                                res.send(JSON.stringify({
                                    "com": "addItem",
                                    "data": {
                                        "ignoremsg": `Tuote lisätty
                                                onnistuneesti`,
                                        updateInventory
                                    }
                                }));

                            }

                        });

                    }
                );

            }

        }
    );

};
