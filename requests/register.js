"use strict";

/*
 * !!!WIP!!! Not to be used by frontend
 * DEF:
 *      This register new user
 * ARGS:
 *      email: New users email
 *      password: New users password
 *
 * EXAMPLE:
 *      email: devmail@mail.com
 *      password: devpass
 *      Now this just adds new row into users table and hashes the password
 *
 *
 */
module.exports = (req, res) => {

    global.db.serialize(() => {

        // Check if user already exists
        global.db.get(`SELECT id
             FROM   users
             WHERE  email='${req.params.email}'`, (existsErr, row) => {

            if (existsErr) {

                console.log(`Register error 1: ${existsErr}`);
                return;

            }
            if (typeof row === "undefined") {

                global.db.run(`INSERT INTO users (
                        email,
                        password,
                        billCount
                     )
                     VALUES (
                        '${req.params.email}',
                        '${global.hashPass(req.params.password)}',
                        1
                    )`, (registerErr) => {

                    if (registerErr) {

                        console.log(`register ERROR: ${err}`);
                        console.log(`Register error 2: ${err}`);
                        if (registerErr === `Error: SQLITE_CONSTRAINT: 
                                UNIQUE constraint failed: users.email at Error 
                                (native)`) {

                            console.log(registerErr);

                        }

                    } else {

                        global.db.run(`INSERT INTO prefill (owner)
                                VALUES ('${req.params.email}')`, (err) => {

                            if (err) {

                                console.log(`Register error prefill: 
                                        ${err}`);

                            }

                        });

                    }

                });
                global.db.run(`INSERT INTO labels (
                        owner,
                        id,
                        quantity,
                        price,
                        barcode,
                        field1,
                        field2,
                        field3,
                        field4,
                        field5,
                        field6,
                        field7,
                        field8,
                        field9,
                        field10,
                        field11,
                        field12,
                        field13,
                        field14,
                        field15,
                        field16,
                        field17,
                        field18,
                        field19,
                        field20,
                        field21,
                        field22,
                        field23,
                        field24,
                        field25
                      )
                     VALUES (
                        '${req.params.email}',
                        'Id',
                        'Määrä',
                        'OVH Hinta',
                        'Barcode',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none',
                        'none'
                      )`, (err) => {

                    if (err) {

                        console.log(`register error 3: ${err}`);

                    }

                });
                res.send(JSON.stringify({
                    "com": "register",
                    "data": "Register propably successfull"
                }));

            } else {

                res.send(JSON.stringify({
                    "com": "register",
                    "data": {
                        "error": "Sähköposti on jo käytössä!"
                    }
                }));

            }

        });

    });

};
