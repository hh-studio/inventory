"use strict";

/*
 * DEF:
 *      Removes employee from employer and also employee user from database
 *
 * ARGS:
 *      employeeEmail: Email of employee to be removed
 *
 * EXAMPLE:
 *      employeeEmail: devmail1@mail.com
 *      We get the employer email from cookie(hes making the request) and remove
 *      all employees from him with email devmail1@mail.com
 *      and also user with email
 *      devmail1@mail.com
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "removeEmployee",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                global.db.run(`DELETE FROM users
                    WHERE email='${req.params.employeeEmail}';`);

                global.db.run(`DELETE FROM labels
                    WHERE owner='${req.params.employeeEmail}';`);

                global.db.run(`DELETE FROM employees
                    WHERE employer='${encoded.data}'
                    AND employee='${req.params.employeeEmail}';`);
                res.send(JSON.stringify({
                    "com": "removeEmployee",
                    "data": "Käyttäjä poistettu onnistuneesti"
                }));

            }

        }
    );

};
