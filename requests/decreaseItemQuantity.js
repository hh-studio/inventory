"Use strict";

/*
 * DEF:
 *      Decreases the quantity of items in database,
 *          doesnt check if it goes under zero
 *
 * ARGS:
 *      itemIds: List of id:s of items to decrease the quantity of
 *      itemQuantities: List of quantities,
 *          of how much quantity should be decreased
 *
 *  EXMAMPLE:
 *      itemIds: [1, 2, 3]
 *      itemQuantities: [1, 2, 1]
 *      Decreases quantity of item with id  1 by 1
 *      Deacreases quantity of item with id 2 by 2
 *      Deacreases quantity of item with id 3 by 1
 *
 *  TODO:
 *      Now item is checked with id only, needs to have real ownership check
 *
 */
module.exports = (req, res) => {

    // Check from cookie that user is logged in
    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        const itemIds = JSON.parse(req.params.itemIds),
                            itemQuantities =
                                JSON.parse(req.params.itemQuantities);
                        console.log(itemIds, itemQuantities);

                        let success = true;
                        itemIds.forEach((itemId, index) => {

                            global.db.run(
                                `UPDATE items
                                 SET    quantity=quantity -
                                    ${itemQuantities[index]}
                                 WHERE  id=${itemId}`,
                                (decreaseItemQuantityErr) => {

                                    console.log(decreaseItemQuantityErr);
                                    if (decreaseItemQuantityErr) {

                                        success = false;

                                        console.log(`decreaseItemCuantity ERROR:
                                            ${decreaseItemQuantityErr}`);

                                    }

                                }
                            );

                        });
                        if (success) {

                            res.send(JSON.stringify({
                                "com": "decreaseItemQuantity",
                                "data": "Tuotteita vähennetty onnistuneesti"
                            }));

                        } else {

                            res.send(JSON.stringify({
                                "com": "decreaseItemQuantity",
                                "data": {
                                    "error": `Tuotteen vähennys
                                        epäonnistui`
                                }
                            }));

                        }


                    }
                );

            }

        }
    );


};
