"use strict";

/*
 *      Returns users or users employers bill count
 *
 * ARGS:
 *
 *  EXMAMPLE:
 *  /api/getBillCount
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({"com": "",
                    "data": {"error": "Et ole kirjautunut sisään"}}));

            } else {

                global.db.get(
                    `SELECT  employer
                     FROM    employees
                     WHERE   employee='${encoded.data}'`,
                    (employerErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        global.db.get(
                            `SELECT    billCount
                             FROM      users
                             WHERE     email='${ownerEmail}'`,
                            (billCountErr, row) => {

                                if (billCountErr) {

                                    res.send(JSON.stringify({
                                        "com": "getBillCount",
                                        "data": {
                                            "error":
                                                "Laskunumeron haku epäonnistui"
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "getBillCount",
                                        "data": {
                                            "billCount": row.billCount
                                        }
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};

