"use strict";

/*
 * DEF:
 *      Removes item from database
 *
 * ARGS:
 *      itemId: id of item to be removed
 *
 * EXAMPLE:
 *      itemId: 1
 *      Item with id 1 will be removed from database
 *
 */

module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "removeItem",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE  employee='${encoded.data}'`,
                    (employerErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }
                        global.db.run(
                            `DELETE FROM items
                             WHERE id='${req.params.itemId}'
                             AND owner='${ownerEmail}'`,
                            (itemIdErr) => {

                                if (itemIdErr) {

                                    console.log(`removeItem ERROR: ${err}`);
                                    res.send(JSON.stringify({
                                        "com": "removeItem",
                                        "data": {
                                            "error": `Tuotteen poistaminen ei
                                                onnistunut, ole hyvä ja yritä
                                                kohta uudelleen.`
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "removeItem",
                                        "data": {
                                            "message": `Tuote poistettu
                                                onnistuneesti.`
                                        }
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );


};
