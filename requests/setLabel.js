"use strict";

/*
 * DEF:
 *  Imports sheet data into the database
 *
 * ARGS:
 *      Secret token to make sure this thing isnt run accidentally
 *
 *  EXMAMPLE:
 *  /api/instantiateInventory/hjuu555
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({"com": "",
                    "data": {"error": "Et ole kirjautunut sisään"}}));

            } else {

                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE  employee='${encoded.data}'`,
                    (employerErr, employerRow) => {

                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }

                        if (encoded.data !== ownerEmail) {

                            res.send(JSON.stringify({
                                "com": "setLabel",
                                "data": {
                                    "error":
                                        `Vain varaston omistaja voi 
                                            asettaa otsikoita`
                                }
                            }));
                            return;

                        }

                        global.db.run(
                            `
          UPDATE
            labels
          SET
            'field${req.params.index}'='${req.params.newLabel}'
          WHERE
            owner='${ownerEmail}'
        `,
                            (err) => {

                                if (err) {

                                    res.send(JSON.stringify({
                                        "com": "setLabel",
                                        "data": {
                                            "error": `Otsikon asettaminen 
                                                epäonnistui: 
                                                    ${req.params.newLabel}`
                                        }
                                    }));

                                } else {

                                    res.send(JSON.stringify({
                                        "com": "setLabel",
                                        "data": {
                                            "ignoremsg": `Otsikon asettaminen 
                                                onnistui: 
                                                ${req.params.newLabel}`
                                        }
                                    }));

                                }

                            }
                        );

                    }
                );

            }

        }
    );

};

