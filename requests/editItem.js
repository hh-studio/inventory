"use strict";

/*
 * DEF:
 *      Adds new values to item into database
 *
 * ARGS:
 *      itemId: The id of the item being modified
 *      newValues: list of new items values
 *
 * EXAMPLE:
 *      itemId: 1
 *      newValues: ["devmail@mail.com", 10, "bc", 0, 12]
 *      This would edit an item to database to have values
 *          owner: devmail@mail.com
 *          quantity: 10
 *          barcode: bc
 *          field1: 0
 *          field2: 12
 *      and the rest will be filled with string "none"
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (err, encoded) => {

            if (err) {

                res.send(JSON.stringify({
                    "com": "editItem",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM   employees
                     WHERE  employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        if (employeeErr) {

                            console.log(`editItem ERROR: ${err}`);
                            return;

                        }
                        let ownerEmail = encoded.data;
                        if (employerRow) {

                            ownerEmail = employerRow.employer;

                        }
                        const newValues = JSON.parse(req.params.newValues);

                        while (newValues.length < 30) {

                            newValues.push("none");

                        }
                        db.run(`UPDATE items
                         SET
                            id = '${newValues[0]}',
                            quantity = '${newValues[1]}',
                            price = '${newValues[2]}',
                            barcode = '${newValues[3]}',
                            field1 = '${newValues[4]}',
                            field2 = '${newValues[5]}',
                            field3 = '${newValues[6]}',
                            field4 = '${newValues[7]}',
                            field5 = '${newValues[8]}',
                            field6 = '${newValues[9]}',
                            field7 = '${newValues[10]}',
                            field8 = '${newValues[11]}',
                            field9 = '${newValues[12]}',
                            field10 = '${newValues[13]}',
                            field11 = '${newValues[13]}',
                            field12 = '${newValues[14]}',
                            field13 = '${newValues[15]}',
                            field14 = '${newValues[16]}',
                            field15 = '${newValues[17]}',
                            field16 = '${newValues[18]}',
                            field17 = '${newValues[19]}',
                            field18 = '${newValues[20]}',
                            field19 = '${newValues[21]}',
                            field20 = '${newValues[22]}',
                            field21 = '${newValues[23]}',
                            field22 = '${newValues[24]}',
                            field23 = '${newValues[25]}',
                            field24 = '${newValues[26]}',
                            field25 = '${newValues[27]}'
                         WHERE id='${newValues[0]}';
                         AND owner='${ownerEmail}'`);

                        res.send(JSON.stringify({
                            "com": "editItem",
                            "data": {
                                "msg": "Values propably changed"
                            }
                        }));

                    }
                );

            }

        }
    );


};
