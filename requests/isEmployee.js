"use strict";

/*
 * DEF:
 *      Returns true if user is employee and false if he is not
 *
 * ARGS:
 *
 *  EXMAMPLE:
 *  /api/isEmployee
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({"com": "",
                    "data": {"error": "Et ole kirjautunut sisään"}}));

            } else {

                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        if (employerRow) {

                            res.send(JSON.stringify({"com": "isEmployee",
                                "data": {"isEmployee": true}}));

                        } else {

                            res.send(JSON.stringify({"com": "isEmployee",
                                "data": {"isEmployee": false}}));

                        }

                    }
                );

            }

        }
    );

};

