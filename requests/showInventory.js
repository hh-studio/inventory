"use strict";

/*
 * DEF:
 *      Gets all users(oremployers) items in invetory and returns them in
 *      a way that inventory page can show them
 *
 * ARGS:
 *      none
 *
 * EXAMPLE:
 *
 */
module.exports = (req, res) => {

    global.jwt.verify(
        req.cookies.hhsSessionToken,
        global.sessionTokenSecret,
        (sessionErr, encoded) => {

            if (sessionErr) {

                res.send(JSON.stringify({
                    "com": "showInventory",
                    "data": {
                        "error": "Et ole kirjautunut sisään"
                    }
                }));

            } else {

                // Check if user has employer
                global.db.get(
                    `SELECT employer
                     FROM employees
                     WHERE employee='${encoded.data}'`,
                    (employeeErr, employerRow) => {

                        let inventoryEmail = encoded.data;
                        if (employerRow) {

                            inventoryEmail = employerRow.employer;

                        } else {

                            inventoryEmail = encoded.data;

                        }

                        global.db.get(
                            `SELECT *
                             FROM labels
                             WHERE owner='${inventoryEmail}' `,
                            (showInventoryLabelsErr, labelsRow) => {

                                const labels = [];
                                Object.keys(labelsRow).forEach((key, index) => {

                                    if (index > 0 &&
                                    labelsRow[key] !== "none" &&
                                    labelsRow[key] !== "Id") {

                                        labels.push({
                                            "title": labelsRow[key]
                                        });

                                    } else if (labelsRow[key] === "Id") {

                                        labels.push({
                                            "title": labelsRow[key],
                                            "visible": false
                                        });

                                    }

                                });

                                global.db.all(
                                    `SELECT *
                                     FROM items
                                     WHERE owner='${inventoryEmail}';`,
                                    (showInventoryErr, itemsRows) => {

                                        const items = [];
                                        itemsRows.forEach((row) => {

                                            const item = [];
                                            Object.keys(row).
                                                forEach((key, index) => {

                                                    /*
                                                     * Change quantity to
                                                     * string from int
                                                     */
                                                    if (index === 0) {

                                                        item.
                                                            push(String(row[key]));

                                                    } else if (index === 1) {
                                                    // Dont push owner
                                                    } else {

                                                        item.push(row[key]);

                                                    }

                                                });
                                            items.push(item);

                                        });

                                        res.send(JSON.stringify({
                                            "com": "showInventory",
                                            "data": {
                                                items,
                                                labels,
                                                "owner": inventoryEmail
                                            }
                                        }));

                                    }
                                );

                            }
                        );

                    }
                );

            }

        }
    );


};
