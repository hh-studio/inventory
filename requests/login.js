"use strict";

/*
 * DEF:
 *      This logs user in,
 *      checks for email and password then gives back a cookie
 *
 * ARGS:
 *      email: Users email
 *      password: Users password
 *
 * EXAMPLE:
 *      email: devmail@mail.com
 *      password: devpass
 *      This would log the user in and return a cookie
 */
module.exports = (req, res) => {

    global.db.get(
        `SELECT password
         FROM users
         WHERE email='${req.params.email}';`,
        (loginErr, row) => {

            // Check if row exists and passwords match
            if (row && row.password === global.hashPass(req.params.password)) {

                res.cookie(
                    "hhsSessionToken",
                    global.genToken(req.params.email), {
                        "expire": global.sessionExpTime()
                    }
                ).
                    send(JSON.stringify({
                        "com": "login",
                        "data": {
                            "email": req.params.email,
                            "message": "Kirjautuminen onnistui"

                        }
                    }));

            } else {

                let errMsg = "Kirjautuminen epäonnistui";
                if (typeof row === "undefined") {

                    errMsg += ": Virheellinnen sähköpostiosoite";

                } else if (row.password !==
                    global.hashPass(req.params.password)) {

                    errMsg += ": Virheellinen salasana";

                }
                res.send(JSON.stringify({
                    "com": "login",
                    "data": {
                        "error": errMsg
                    }
                }));

            }

        }
    );

};
