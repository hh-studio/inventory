"use strict";

Vue.component("home", {
    "props": ["page"],
    "template": `
        <div id="home" class="page" v-show="page === 'home'">
            Home
        </div>
    `
});
