"use strict";

Vue.component("add-item", {
    "props": [
        "labels",
        "addItem",
        "addItemBarcode"
    ],
    "template": `
        <div id="modal-add-item" class="modal" tabindex="-1" role="dialog">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h2 class="modal-title pull-left">

                            Lisää tuote

                        </h2>
                        <button
                            type="button"
                            class="close pull-right"
                            data-dismiss="modal"
                            >

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                    <div class="modal-body">

                        <div v-for="(label, index) in labels">

                            <label
                                v-bind:for="label"
                                v-show="label !== 'none' && index > 0"
                                >

                                {{ label }}

                            </label>

                            <input
                                v-bind:id="'modal-add-item-' + label"
                                class="form-control"
                                v-show="label !== 'none' && index > 0">

                            </div>

                        </div>

                        <div class="modal-footer">

                            <button
                                type="button"
                                class="btn btn-primary"
                                v-on:click="addItem"
                                data-dismiss="modal"
                                >

                                    Lisää
                            </button>
                            <button
                                type="button"
                                class="btn btn-secondary"
                                v-on:click="addItemBarcode"
                                >

                                Lue viivakoodi

                            </button>

                        </div>

                    </div>

                </div>

            </div>
    `
});
