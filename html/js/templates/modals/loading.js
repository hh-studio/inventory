"use strict";

Vue.component("modal-loading", {
    "props": ["loadingInfo"],
    "template": `
        <div id="modal-loading" class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <img id="modal-loading-gif" src="img/45.gif"></img>
                <div class="text-center"> {{ loadingInfo }} </div>
              </div>
            </div>
          </div>
        </div>
    `
});
