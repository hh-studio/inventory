"use strict";

Vue.component("barcode", {
    "props": [],
    "template": `
        <div id="modal-barcode" class="modal" tabindex="-1" role="dialog">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h2 class="modal-title pull-left">Lue viivakoodi</h2>
                        <button
                            type="button"
                            class="close pull-right"
                            data-dismiss="modal"
                            >

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                    <div class="modal-body" style="height:35em;">

                        <div id="barcode-video" class="col-xs-12 well"></div>

                    </div>

                </div>

            </div>

        </div>
    `
});
