"use strict";

Vue.component("stats", {
    "props": [
        "stats"
    ],
    "template": `
        <div id="modal-stats" class="modal" tabindex="-1" role="dialog">

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h2 class="modal-title pull-left">Statistiikkaa</h2>
                        <button
                            type="button"
                            class="close pull-right"
                            data-dismiss="modal"
                            >

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                    <div class="modal-body">

                        <table class="table">

                            <tr v-for="key in Object.keys(stats)">

                                <td>

                                    {{ key }}

                                </td>

                                <td>

                                    {{ stats[key] }}

                                </td>

                            </tr>

                        </table>

                    </div>

                </div>

            </div>

        </div>
    `
});
