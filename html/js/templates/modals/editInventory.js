"use strict";

Vue.component("edit-inventory", {
    "props": [
        "itemEditedNow",
        "editInventoryItem",
        "editItemBarcode",
        "labels",
        "removeInventoryItem"
    ],
    "template": `
        <div
            id="modal-edit-inventory"
            class="modal"
            tabindex="-1"
            role="dialog"
            >

            <div class="modal-dialog" role="document">

                <div class="modal-content">

                    <div class="modal-header">

                        <h2 class="modal-title pull-left">Muokkaa</h2>
                        <button
                            type="button"
                            class="close pull-right"
                            data-dismiss="modal"
                            >

                            <span aria-hidden="true">&times;</span>

                        </button>

                    </div>

                    <div class="modal-body">

                        <input
                            type="hidden"
                            id="modal-edit-item-Id"
                            v-bind:value="itemEditedNow[0]"
                            ></input>

                        <div v-for="(label, index) in labels">
                        
                            <label
                                v-bind:for="label"
                                v-show="label !== 'none' && index > 0"
                                >

                                {{ label }}

                            </label>

                            <input
                                class="form-control"
                                v-bind:id="'modal-edit-item-' + label"
                                v-bind:value="itemEditedNow[index]"
                                v-show="label !== 'none' && index > 0"
                                >

                            </div>

                        </input>

                    </div>

                    <div class="modal-footer">

                        <button
                            type="button"
                            class="btn btn-primary"
                            v-on:click="editInventoryItem"
                            data-dismiss="modal">

                                Tallenna

                            </button>
                        <button
                            type="button"
                            class="btn btn-secondary"
                            v-on:click="editItemBarcode">

                                Lue viivakoodi

                            </button>
                        <button
                            type="button"
                            class="btn btn-secondary"
                            v-on:click="removeInventoryItem(itemEditedNow[0])">

                                Poista

                            </button>

                    </div>

                </div>

            </div>

        </div>
    `
});
