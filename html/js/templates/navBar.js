"use strict";

Vue.component("nav-bar", {
    "props": [
        "page",
        "loggedIn",
        "cart",
        "email",
        "userIsEmployee",
        "logout",
        "menuTitle",
        "changePage"
    ],
    "template": `
        <nav class="navbar navbar-default">

            <div class="container">

                <!-- Brand and toggle get grouped
                for better mobile display asd-->
                <div class="navbar-header">

                    <button
                        type="button"
                        class="navbar-toggle collapsed"
                        data-toggle="collapse"
                        data-target="#collapse-1"
                        aria-expanded="false"
                    >

                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <!--hamburger effect-->
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>

                    <a href="#home" v-on:click="changePage('home')"></a>

                    <a
                        class="navbar-brand"
                        href="#home"
                        v-on:click="changePage('home')"
                    >
                            {{ menuTitle }}
                    </a>

                </div>

                <!-- Collect the nav links, forms,
                    and other content for toggling -->
                <div class="collapse navbar-collapse" id="collapse-1">

                    <ul class="nav navbar-nav">

                        <li class="active" v-show="!loggedIn">

                            <a
                                href="#login"
                                v-on:click="changePage('login')"
                                data-toggle="collapse"
                                data-target=".navbar-collapse"
                                >

                                <span
                                    class="glyphicon glyphicon-log-in"
                                    aria-hidden="true"
                                ></span>

                            Kirjaudu

                        </a>
                    </li>

                    <!--<span class="sr-only">(current)</span>-->
                    <li v-show="loggedIn">

                        <a
                            href="#inventory"
                            v-on:click="changePage('inventory')"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                            >

                            <span
                                class="glyphicon glyphicon-th-list"
                                aria-hidden="true"
                                ></span>
                            Varasto

                        </a>

                    </li>

                    <li id="menu-li-1" v-show="loggedIn">

                        <a
                            href="#cart"
                            v-on:click="changePage('cart')"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                        >

                            <span
                                class=
                                "glyphicon glyphicon-shopping-cart">
                            </span>

                            <span v-show="cart.length > 0">

                                ({{ cart.length }})

                            </span>

                            Ostoskärry

                        </a>

                    </li>

                    <li v-show="loggedIn && userIsEmployee == false">

                        <a
                            href="#employees"
                            v-on:click="changePage('employees')"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                        >

                            <span
                                class="glyphicon glyphicon-user"
                                aria-hidden="true"
                                ></span>
                            Käyttäjät

                        </a>

                    </li>

                    <li id="menu-li-1" v-show="loggedIn">

                        <a
                            href="#import"
                            v-on:click="changePage('settings')"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                        >

                            <span
                                class="glyphicon glyphicon-home"
                                aria-hidden="true"
                                ></span>
                            Asiakkaat

                        </a>

                    </li>

                    <li
                        v-show="loggedIn && userIsEmployee == false"
                    >
                            <a
                                href="#import"
                                v-on:click="changePage('settings')"
                                data-toggle="collapse"
                                data-target=".navbar-collapse"
                            >

                                <span
                                    class="glyphicon glyphicon-cog"
                                    aria-hidden="true"
                                    ></span>
                                Asetukset

                            </a>

                        </li>

                        <li v-show="loggedIn && userIsEmployee == false">

                            <a
                                href="#import"
                                v-on:click="changePage('import')"
                                data-toggle="collapse"
                                data-target=".navbar-collapse"
                            >

                                <span
                                    class="glyphicon glyphicon-import"
                                    aria-hidden="true"
                                    ></span>
                                Tuo

                            </a>

                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right">

                        <li v-show="loggedIn">

                            <a
                                href="#"
                                v-on:click="logout"
                                title="Kirjaudu ulos"
                            >

                                <span
                                    class="glyphicon glyphicon-log-out"
                                ></span>

                            {{ email }}

                        </a>

                    </li>

                </ul>

            </div>

            <!-- /.navbar-collapse -->
        </div>

        <!-- /.container-fluid -->
    </nav>
    `
});
