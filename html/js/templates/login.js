"use strict";

Vue.component("login", {
    "props": [
        "page",
        "login"
    ],
    "template": `
        <div id="login" class="page" v-show="page === 'login'">
            <div class="well col-xs-12 col-md-6 col-md-offset-3">
                <h2> Kirjaudu </h2>

                <div class="form-group">
                    <label for="login-email">
                        Sähköposti
                    </label>
                    <input
                        id="login-email"
                        type="email"
                        class="form-control" @keyup.enter="login"
                    ></input>
                </div>

                <div class="form-group">
                    <label for="login-password">
                        Salasana
                    </label>
                    <input
                        id="login-password"
                        type="password"
                        class="form-control"
                        @keyup.enter="login"
                    ></input>
                </div>

                <input
                    type="button"
                    value="Kirjaudu"
                    class="btn btn-primary"
                    v-on:click="login"
                ></input>

            </div>
        </div>
    `
});
