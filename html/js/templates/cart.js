"use strict";

Vue.component("cart", {
    "props": [
        "page",
        "emptyCart",
        "cart",
        "cartTableAdditionalCellsTitles",
        "cartBillInfoTitles",
        "billSellerData",
        "billBuyerData",
        "billAdditionalInfo",
        "labels",
        "cartBillInfoData",
        "billSellerData",
        "billBuyerData",
        "cartItemAdditionalCellsTitles",
        "cartTableAdditionalCellsData",
        "billAdditionalInfo",
        "printBill",
        "emptyCart",
        "emptyCartAndDecreasItems",
        "cartToBill",
        "cartCalculations",
        "minMaxVisual"
    ],
    "template": `
        <div id="cart" class="page row" v-show="page === 'cart'">

            <div>

                <div id="cart-cart" class="well col-xs-12">

                    <h2>

                        <span class="glyphicon glyphicon-shopping-cart"></span>
                        Ostoskärry
                        <input
                            class="btn"
                            type="button"
                            v-on:click="emptyCart"
                            value="Tyhjennä"
                            ></input>

                    </h2>

                    <div id="cart-content">

                        <div
                            v-for="(item, itemIndex) in cart"
                            class="col-xs-12 col-md-6"
                            >

                            <div class="panel panel-default">

                                <div class="panel-heading">

                                    <span
                                        v-for="(field, index) in item.itemData"
                                        v-show="field !== 'none' &&
                                            index > 3 &&
                                            index < 6"
                                            >
                                        {{ field }}
                                    </span>
                                    <button
                                        type="button"
                                        class="close"
                                        v-on:click=
                                            "removeFromCart(item.itemData[0])
                                        >

                                        <span aria-hidden="true">&times;</span>

                                    </button>

                                </div>

                                <div class="panel-body">

                                    <div>

                                        Määrä:
                                        <input
                                            class="form-control"
                                            type="number"
                                            min="1"
                                            v-bind:max="item.itemData[1]"
                                            v-model:value="item.quantity"
                                            v:bind:id="'cart-quantity-'
                                                + item.itemData[0]"
                                            v-on:change="minMaxVisual"
                                            v-on:input="minMaxVisual"
                                            >
                                        </input>

                                    </div>

                                    <div>

                                        OVH Hinta:
                                        <input
                                            class="form-control"
                                            type="number" min="1"
                                            v-bind:max="item.itemData[0]"
                                            v-model:value="item.itemData[2]"
                                            v:bind:id="'cart-quantity-' +
                                                item.itemData[0]"
                                            v-on:change="cartCalculations"
                                            v-on:input="cartCalculations"
                                            >
                                        </input>

                                    </div>

                                    <div
                                        v-for="(cell, index) in
                                            cartItemAdditionalCellsTitles"
                                        >

                                        {{ cell }}
                                        <input
                                            v-bind:readonly="index > 1"
                                            class="form-control"
                                            type="number"
                                            v:bind:id=
                                                "'cartItemAdditionalCells-' +
                                                    index"
                                            v-model:value="item.cartData[index]"
                                            v-on:change="cartCalculations"
                                            v-on:input="cartCalculations"
                                            >
                                        </input>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div
                    id="cart-bill-info"
                    class="well col-xs-12" style="display:none;"
                    >

                    <h2>Laskun loppusummat</h2>

                    <div>

                        <div
                            v-for="(title, index) in
                                cartTableAdditionalCellsTitles"
                            >

                            {{ title }}
                            <input
                                type="text"
                                class="form-control"
                                v-bind:id="'cart-table-additional-cells-data-' +
                                    index"
                                v-model.text=
                                    "cartTableAdditionalCellsData[index]"
                                >
                            </input>

                        </div>

                    </div>

                </div>


                <div id="cart-bill-info" class="well col-xs-12">

                    <h2>Laskun tiedot</h2>

                    <div
                        class="form-group"
                        v-for="(title, index) in cartBillInfoTitles"
                        >

                        <label for="cart-bill-id">{{ title }}</label>
                        <input
                            class="form-control"
                            v-bind:id="'cart-bill-info-' + index"
                            type="text"
                            v-model.text="cartBillInfoData[index]"
                            >
                        </input>

                    </div>

                </div>

                <div id="cart-bill-info" class="well col-xs-12">

                    <h2>Myyjän tiedot</h2>

                    <div v-for="(line, index) in billSellerData">

                        <input
                            class="form-control"
                            type="text"
                            v-model.text="billSellerData[index]"
                            v-bind:id="'seller-data-' + index"
                            >
                        </input>

                    </div>

                </div>

                <div id="cart-bill-info" class="well col-xs-12">

                    <h2>Ostajan tiedot</h2>

                    <div v-for="(line, index) in billBuyerData">
                        <input
                            class="form-control"
                            type="text"
                            v-model.text="billBuyerData[index]"
                            v-bind:id="'buyer-data-' + index"
                            >
                        </input>
                    </div>

                </div>

                <div id="cart-bill-info" class="well col-xs-12 col-md-12">

                    <h2>Laskun lisätiedot</h2>
                    <textarea
                        class="col-xs-12 form-control"
                        v-model.text="billAdditionalInfo"
                        v:bind:id="rdzcxcsdsdsds + index"
                        >

                    </textarea>

                </div>

                <div id="cart-bill-info" class="well col-xs-12 col-md-12">

                    <h2>Laskun näkyvät solut</h2>

                    <div class="cart-labels">

                        <span v-for="(label, index) in labels">

                            <div v-show="index > 1" class="checkbox">

                                <label
                                    class="btn btn-default"
                                    style="padding-right: 25px;"
                                    v-bind:for="'cart-checkbox-' + index"
                                    >
                                    {{ label }}
                                </label>
                                <input
                                    style="height: 100%;"
                                    type="checkbox"
                                    name="label"
                                    v-bind:id="'cart-checkbox-' + index"
                                    v-model="cartToBill[index]"
                                    v-bind:checked="cartToBill[index]"
                                    ></input>

                            </div>

                        </span>

                    </div>

                </div>


                <div id="cart-bill" class="well col-xs-12">

                    <h2>Lasku</h2>

                    <div id="bill">

                        <div id="bill-bill-data">

                            <div
                                class="bill-info-item"
                                v-for="(item, index) in cartBillInfoData"
                                v-bind:id="'bill-data-' + index"
                                >

                                {{ cartBillInfoTitles[index] }}
                                <span class="f-right">
                                    {{ item }}
                                </span>

                            </div>

                        </div>

                        <div id="bill-seller-data">
                            <div v-for="line in billSellerData">
                                {{ line }}
                            </div>
                        </div>

                        <div id="bill-buyer-data">
                            <div v-for="line in billBuyerData">
                                {{ line }}
                            </div>
                        </div>

                        <table id="bill-table" width="100%">
                            <thead>
                                <tr>

                                    <td
                                        v-for="(cell, index) in labels"
                                        v-show="cell !== 'none' &&
                                            cartToBill[index] == true">

                                        {{ cell }}

                                    </td>

                                    <td>Määrä</td>

                                    <td
                                        v-for="(cell, index) in
                                            cartItemAdditionalCellsTitles">

                                        {{ cell }}

                                    </td>

                                </tr>

                            </thead>

                            <tbody>

                                <tr v-for="(item, itemIndex) in cart">

                                    <td
                                        v-for="(cell, index) in item.itemData"
                                        v-show="cell !== 'none' &&
                                            cartToBill[index] == true"
                                        >

                                        {{ cell }}

                                    </td>

                                    <td>

                                        {{ item.quantity }}

                                    </td>

                                    <td v-for="(cell, index) in item.cartData">

                                        {{ cell }}

                                    </td>


                                </tr>

                            </tbody>

                        </table>

                        <div id="bill-table-additional-cells">

                            <div
                                v-for="(item, index) in
                                    cartTableAdditionalCellsData"
                                v-bind:id="'bill-table-additonal-cell-' +
                                    index"
                                v-show="index != 1"
                                >

                                {{ cartTableAdditionalCellsTitles[index] }}

                                <span class="f-right">

                                    {{ item }}

                                </span>

                            </div>

                        </div>

                        <div id="bill-additional-info">

                            {{ billAdditionalInfo }}

                        </div>
                    </div>
                </div>

                <div id="cart-menu" class="well col-xs-12">

                    <input
                        type="button"
                        v-on:click="printBill"
                        value="Tulosta"
                        class="btn btn-primary"
                        >
                    </input>

                    <input
                        type="button"
                        v-on:click="emptyCartAndDecreasItems"
                        value="Tyhjennä kori ja vähennä tuotteet inventaariosta"
                        class="btn btn-primary">
                    </input>

                </div>

            </div>

        </div>
    `
});
