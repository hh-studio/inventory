"use strict";

Vue.component("settings", {
    "props": [
        "page",
        "savePrefill",
        "billSellerData",
        "cartBillInfoTitles",
        "cartBillInfoData"
    ],
    "template": `
        <div id="settings" class="page" v-show="page === 'settings'">
            <div class="well col-xs-12">
                <h2>Esitäyttö</h2>

                <h3>Myyjän tiedot</h3>

                <div v-for="(line, index) in billSellerData">
                    <input
                        class="form-control" type="text"
                        v-model.text="billSellerData[index]"
                        v-bind:id="'prefill-seller-data-' + index">
                    </div>

                    <h3>Laskun tiedot</h3>

                    <div
                        class="form-group"
                        v-for="(title, index)
                        in cartBillInfoTitles" v-show="index > 3">
                        <label for="cart-bill-id">{{ title }}</label>
                        <input
                            class="form-control"
                            v-bind:id="'cart-bill-info-' + index"
                            type="text"
                            v-model.text="cartBillInfoData[index]"
                        >
                            </input>
                        </div>

                        <input
                            class="btn"
                            type="button"
                            v-on:click="savePrefill"
                            value="Tallenna">
                        </input>
                    </div>
                </div>
        `
});
