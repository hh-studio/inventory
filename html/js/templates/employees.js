"use strict";

Vue.component("employees", {
    "props": [
        "page",
        "addEmployee",
        "getEmployees",
        "employees"
    ],
    "template": `
        <div id="employees" class="page" v-show="page === 'employees'">
            <div id="add-customer" class="well">
                <h2>Lisää käyttäjä</h2>

                <form>
                    <div class="form-group">
                        <label for="add-employee-email">Sähköposti</label>
                        <input
                            id="add-employee-email"
                            type="text"
                            class="form-control"
                        ></input>
                    </div>
                    <div class="form-group">
                        <label for="add-employee-password">Salasana</label>
                        <input
                            id="add-employee-password"
                            type="password"
                            class="form-control"
                        ></input>
                    </div>
                    <div class="form-group">
                        <label for="add-employee-password1">
                            Salasana uudestaan
                        </label>
                        <input
                            id="add-employee-password1"
                            type="password"
                            class="form-control"
                        ></input>
                    </div>
                    <input
                        type="submit"
                        value="Lisää käyttäjä"
                        class="btn btn-primary"
                        v-on:click="addEmployee"
                    ></input>
                </form>
            </div>
            <div id="employees-list" class="well">
                <h2 style="margin-bottom: 25px">
                    Käyttäjät
                </h2>
                <ul>
                    <div
                        v-for="employee in employees"
                        class="col-xs-12" style="margin-bottom: 25px"
                    >
                        {{ employee.email }}
                        <input
                            type="button"
                            class="btn btn-default"
                            value="Poista"
                            v-on:click="removeEmployee(employee.email)"
                            style="float: right"
                        ></input>
                    </div>
                </ul>
                <div>
                    <input
                        type="button"
                        class="btn btn-default"
                        value="Päivitä"
                        v-on:click="getEmployees"
                    ></input>
                </div>
            </div>
        </div>
    `
});
