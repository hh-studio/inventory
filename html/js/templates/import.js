"use strict";

Vue.component("import", {
    "props": [
        "page",
        "loadSheet",
        "startImportSheet"
    ],
    "template": `
        <div id="import" class="page" v-show="page === 'import'">
            <div class="well col-xs-12">
                <h2>Ohje</h2>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Hinta, määrä ja Barcode</h4>
                    </div>
                    <div class="panel-body">
                        <img
                            class="card-img-top"
                            src="./img/exampleimport-quantity-price-barcode.png"
                        />
                        <p>Taulukon ensimmäiset kolme saraketta tulee olla:</p>
                        <p>Määrä(A rivi), Hinta(B rivi) ja Barcode(C rivi)</p>
                        <p>Jos taulukossa ei ennestään ole näitä sarakkeita
                            niiden arvot voidaan jättää tyhjäksi lukuunottamatta
                            nimeä(Rivi 1)</p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Otsikot</h4>
                    </div>
                    <div class="panel-body">
                        <img src="./img/exampleimport-column-titles.png" />
                        <p>Jokaisen sarakkeen 1. rivi kertoo sarakkeen otsikon.
                            Näitä voi olla maksimissaan 24(Hinnan, määrän ja
                            barcoden lisäksi)
                        </p>
                        <p>Jos otsikkosolu(A1, B1, C1...) on tyhjä, rivi
                            jätetään huomioimatta
                        </p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Esimerkki</h4>
                    </div>
                    <div class="panel-body">
                        <img src="./img/exampleimport-item.png" />
                        <p>Hyväksytyt tiedostomuodot ovat <b>.xlsx</b> ja
                            <b>.ods</b></p>
                    </div>
                </div>

            </div>

            <div class="well col-xs-12">
                <h2>Tiedosto</h2>
                <input
                    name="sheetToImport"
                    id="sheetToImport"
                    type="file"
                    v-on:change="loadSheet"
                />
            </div>

            <div class="well col-xs-12">
                <h2>Esikatselu</h2>
                <!--- We init import preview list into this table,
                    DONT CHANGE--->
                <table
                    id="import-preview-list"
                    class="table table-striped table-bordered"
                    width="100%"
                >
                </table>
            </div>

            <div class="well col-xs-12" if="sheetPreviewOn === true">
                <b>Huomio: taulukon tuonti varastoon tyhentää varaston sekä
                    tiedoista, että otsikoista.</b>
            </div>

            <div class="well col-xs-12" if="sheetPreviewOn === true">
                <input
                    type="button"
                    class="btn btn-primary"
                    value="Tuo"
                    v-on:click="startImportSheet"
                ></input>
            </div>
        </div>
    `
});
