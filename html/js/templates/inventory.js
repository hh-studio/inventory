"use strict";

Vue.component("inventory", {
    "props": ["page"],
    "template": `
        <div id="inventory" class="page" v-show="page === 'inventory'">
            <div class="well col-xs-12">
                <!--- We init inventory list into this table, dont change --->
                <table
                    id="inventory-list"
                    class="table table-striped table-bordered"
                    width="100%"
                ></table>
            </div>
        </div>
    `
});
