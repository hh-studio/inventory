"use strict";

const appWatch = {

    "cartBillInfoData" (newCBIF) {

        /*
         * Maksuehto, term
         * eräpäivä, due date
         */
        // Const dueDate = this.cartBillInfoData[3];

        const calculateDueDate =
                function calculateDueDate (term, billDate) {

                    return moment(billDate, "DD.MM.YYYY").
                        add(parseInt(term, 10), "days").
                        format("DD.MM.YYYY");

                };

        this.cartBillInfoData[3] = calculateDueDate(newCBIF[4], newCBIF[0]);

    },

    "cartTableAdditionalCellsData" () {

        this.cartCalculations();

    }
};
