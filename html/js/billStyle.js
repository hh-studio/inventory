/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * IF YOU UPDATE THIS ALSO UPDATE style.css TO MATCH!!!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
const billStyle = `
@page {
    size: auto;  /* auto is the initial value */
    margin: 3mm; /* this affects the margin in the printer settings */
}
html {
    background-color: #FFFFFF;
    margin: 5px 5px 5px 5px; /* this affects the margin on the HTML before sending to printer */
}
body {
}

.f-right {
    float: right;
}

#bill {
  height: 100%;
  position: relative;
  background-color: white;
  color: black;
  padding: 0 0 0 0;
}

#bill-bill-data {
  width: 50%;
  float: right;
  padding-bottom: 50px;
    line-height: 1.4em;
}

#bill-additional-info {
  word-break: break-all;
  width: 45%;
  position: absolute;
  bottom: 0;
  left: 0;
}

#bill-seller-data {
  width: 50%;
  float: left;
  padding-bottom: 25px;
}

#bill-buyer-data {
  width: 50%;
  float: left;
}


#bill-table {
  border: 1px solid black;
  width: 100%;
}

#bill-table > thead {
  border: 1px solid black;
  background-color: #FFCC00;

}

#bill-table > tbody > tr {
  border: 1px solid black;
}

#bill-table > tbody > tr > td {
  border: 1px solid black;
  text-align: center;
}


#bill-table-additional-cells {
  word-break: break-all;
  width: 45%;
  position: absolute;
  bottom: 0;
  right: 0;
}
`;
