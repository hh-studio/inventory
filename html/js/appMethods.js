"use strict";

const appMethods = {

    "emphasizeElement" (elementId) {

        const elem = document.getElementById(elementId),
            emphasizeTime = 500;

        elem.style.backgroundColor = "green";
        setTimeout(() => {

            elem.style.backgroundColor = "";

        }, emphasizeTime);

    },

    "getEmployeeStatus" () {

        if (this.userIsEmployee === "none") {

            axios.get(`${this.apiURL}/isEmployee`).
                then((response) => {

                    const responseData = response.data.data;
                    if (this.noResponseErrors(responseData)) {

                        this.userIsEmployee = response.data.data.isEmployee;

                    }

                }).
                catch((userIsEmployeeErr) => {

                    console.log(`userIsEmployee ERROR: ${userIsEmployeeErr}`);

                });

        }

    },

    "getPrefill" () {

        axios.get(`${this.apiURL}/getPrefill`).
            then((response) => {

                const responseData = response.data.data;

                if (this.noResponseErrors(responseData)) {

                    if (responseData.seller && responseData.seller !== "null") {

                        this.billSellerData = JSON.parse(responseData.seller);

                    }
                    if (responseData.cartBillInfoData &&
                        responseData.seller !== "null") {

                        this.cartBillInfoData =
                        JSON.parse(responseData.cartBillInfoData);

                    }

                }

            }).
            catch((getPrefillErr) => {

                console.log(`getPrefill ERROR: ${getPrefillErr}`);

            });


    },

    "minMaxVisual" (event) {

        if (
            event.srcElement.value < event.srcElement.min ||
            event.srcElement.value > event.srcElement.max) {

            event.srcElement.style.color = "red";

        } else {

            event.srcElement.style.color = "black";

        }
        this.cartCalculations();

    },

    "savePrefill" () {

        const billSellerDataEscaped = encodeURIComponent(JSON.stringify({
            "cartBillInfoData": this.cartBillInfoData,
            "seller": this.billSellerData
        }));
        axios.get(`${this.apiURL}/setPrefill/${billSellerDataEscaped}`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {

                    if (responseData.msg === "Success") {

                        alert("Esitäyttö tallennettu onnistuneesti");

                    } else {

                        alert(`Jokin meni vikaan,
                            esitäyttöä ei voitu tallentaa`);

                    }

                }

            }).
            catch((setPrefillErr) => {

                console.log(`setPrefill ERROR: ${setPrefillErr}`);

            });

    },


    "showStats" () {

        const itemsStats = [],
            multiplyByQuantity =
            function multiplyByQuantity (quantity, value) {

                return quantity * value;

            },
            stats = [];

        // Multiple numerical item values by its quantity
        this.inventoryItems.forEach((item) => {

            const itemStats = {};
            this.inventoryLabels.forEach((label, index) => {

                if (!isNaN(item[index]) && index > 1) {

                    itemStats[label.title] =
                        multiplyByQuantity(item[index], item[1]);

                }

            });
            itemsStats.push(itemStats);

        });

        // Add all items multiplied values together
        itemsStats.forEach((item) => {

            Object.keys(item).forEach((key) => {

                if (!stats[`${key} yhteensä`]) {

                    stats[`${key} yhteensä`] = 0;

                }
                stats[`${key} yhteensä`] += item[key];

            });

        });
        this.stats = stats;
        $("#modal-stats").modal("show");

    },


    "cartCalculations" () {

        function discountPriceWithoutTax (item) {

            const itemPrice = item.cartData[2],
                discountPercent = item.cartData[0],
                discount = itemPrice * parseFloat(discountPercent / 100),
                itemPriceWithDiscount = itemPrice - discount,
                tax = item.cartData[1];
            return parseFloat(itemPriceWithDiscount / `1.${tax}`).toFixed(2);

        }

        function discountPriceWithTax (item) {

            const itemPrice = item.cartData[2],
                discountPercent = item.cartData[0],
                discount = itemPrice * parseFloat(discountPercent / 100),
                itemPriceWithDiscount = itemPrice - discount,
                tax = item.cartData[1];
            return itemPriceWithDiscount;

        }

        function taxAmount (item) {

            return discountPriceWithTax(item) - discountPriceWithoutTax(item);

        }

        // Items taxes
        this.cart.forEach((item) => {

            item.cartData[2] = item.itemData[2];
            item.cartData[3] = discountPriceWithTax(item);
            item.cartData[4] = discountPriceWithoutTax(item);

        });

        // Sums
        let sumWithoutTaxes = 0,
            taxesSum = 0;
        this.cart.forEach((item) => {

            sumWithoutTaxes += item.quantity * parseFloat(item.cartData[4]);

        });
        this.cartTableAdditionalCellsData[0] =
            parseFloat(sumWithoutTaxes).toFixed(2);

        this.cart.forEach((item) => {

            taxesSum += item.quantity * taxAmount(item);

        });
        this.cartTableAdditionalCellsData[2] = parseFloat(taxesSum).toFixed(2);

        // Sum of taxes and sum of prices added together
        this.cartTableAdditionalCellsData[3] = sumWithoutTaxes + taxesSum;


        // Taxes
        /*
         * Var taxPercent = parseFloat(this.cartTableAdditionalCellsData[1]);
         * var cartSum = parseFloat(this.cartTableAdditionalCellsData[0]);
         * var tax = cartSum * (taxPercent / 100);
         * var sumWithTax = parseFloat(cartSum + tax);
         * this.cartTableAdditionalCellsData[2] = tax.toFixed(2);
         * this.cartTableAdditionalCellsData[3] = sumWithTax.toFixed(2);
         */


    },


    /*
     * This function is run on every page load at the beginning
     *
     */
    "init" () {

        const self = this;
        moment.locale("FIN");

        window.onhashchange = function onhashchange () {

            self.updatePageFromHash();

        };

        $.fn.dataTable.ext.errMode = "none";

        // Get session token from cookie
        document.cookie.split(";").forEach((key) => {

            const cookie = key.replace(" ", "").split("=");

            if (cookie[0] == "hhsSessionToken") {

                self.sessionToken = cookie[1];
                self.loggedIn = true;

            } else {

                self.changePage("login");

            }

        });

        if (window.location.hash) {

            self.changePage(window.location.hash.replace("#", ""));

        }

        this.changeInventoryItemClickType("addToCart");

        $("#modal-barcode").on("hidden.bs.modal", () => {

            if (Quagga) {

                Quagga.stop();

            }

        });

        this.getEmployeeStatus();
        setTimeout(this.getPrefill(), 1000);
        document.getElementById("app").style.display = "block";
        document.getElementById("loading-message").style.display = "none";

    },


    "startImportSheet" () {

        this.cart = [];
        const self = this;
        if (confirm(`Oletko varma, että haluat tuoda taulukon ohjelmaan.
            Taulukon tuonti tyhjentää varaston edellisistä 
            tiedoista ja otsikoista`)) {

            if (this.sheetImportData === "") {

                alert("Taulukkotiedostoa ei ole ladattu ohjelmaan");

            } else {

                $("#modal-loading").modal("show");
                this.loadingInfo = "Alustetaan varastoa";
                // Wait a little bit so it looks better to user :D
                setTimeout(() => {

                    axios.get(`${this.apiURL}/instantiateInventory/hjuu555`).
                        then((response) => {

                            const responseData = response.data.data;
                            if (this.noResponseErrors(responseData)) {

                                this.importSheetLabels();

                            }

                        }).
                        catch((instantiateInventoryErr) => {

                            console.log(`instantiateInventory ERROR:
                                ${instantiateInventoryErr}`);

                        });


                }, 2000);

            }

        }

    },

    "importSheetLabels" () {

        const self = this;
        this.loadingInfo = "Kirjoitetaan otsikoita";
        this.sheetImportData.labels.forEach((label, index) => {

            // First 3 are quantity, price and barcode
            if (index > 2) {

                axios.get(`${this.apiURL}/${"setLabel/"}${index - 2}/
                    ${label.title}`).then((response) => {

                    const responseData = response.data.data;
                    if (this.noResponseErrors(responseData)) {
                    }

                }).
                    catch((setLabelErr) => {

                        console.log(`setLabel ERROR: ${setLabelErr}`);

                    });

            }

        });
        this.importSheetItems();

    },


    "importSheetItems" () {

        const self = this;
        this.loadingInfo = "Lisätään tuotteita varastoon";
        this.sheetImportData.data.forEach((item) => {

            if (item.length > 0) {

                item.forEach((itemValue) => {

                    if (!itemValue) {

                        itemValue = "none";

                    }

                });
                self.addItem(item);

            }

        });
        setTimeout(() => {

            self.finishSheetImport();

        }, 4000);

    },

    "finishSheetImport" () {

        $("#modal-loading").modal("hide");
        const table = $("#import-preview-list").DataTable();
        table.destroy();
        this.sheetImportData = "";
        this.sheetPreviewOn = false;
        this.loadingInfo = "";
        this.changePage("inventory");

    },

    /*
     * This takes the excel file and chnages it so we can display it in
     * datatables
     * Then its puts it into importSheetData variable
     */
    "loadSheet" (e) {

        if ($.fn.dataTable.isDataTable("#import-preview-list")) {

            const table = $("#import-preview-list").DataTable();
            table.destroy();

        }

        // True: readAsBinaryString ; false: readAsArrayBuffer
        const files = e.target.files,
            f = files[0],
            rABS = true,
            reader = new FileReader();

        reader.onload = function onload (e) {

            let data = e.target.result;
            if (!rABS) {

                data = new Uint8Array(data);

            }

            /*
             * First we put every column into its own array
             * using the letter(key[0])
             */
            const charArrays = {},

                /*
                 * Console.log(workbook);
                 * Change workbook data to be compatible with datatables data
                 */
                wbdt = {
                    "data": [],
                    "labels": []
                },
                workbook = XLSX.read(data, {
                    "type": rABS
                        ? "binary"
                        : "array"
                });


            Object.keys(workbook.Sheets).forEach((sheetKey, index) => {

                if (index === 0) {

                    Object.keys(workbook.Sheets[sheetKey]).forEach((key) => {

                        if (!charArrays[key[0]]) {

                            charArrays[key[0]] = [];

                        }
                        charArrays[key[0]].push(workbook.Sheets[sheetKey][key]);

                    });

                }

            });
            // Console.log(charArrays);

            // See which one is the longest
            let longestLength = 0,
                longestLetter = "";
            Object.keys(charArrays).forEach((letter) => {

                if (charArrays[letter].length > longestLength) {

                    longestLength = charArrays[letter].length;
                    longestLetter = letter;

                }

            });

            // Make a mix of every array thats as long as the longest
            const items = [];
            charArrays[longestLetter].forEach((item, index) => {

                item = [];
                Object.keys(charArrays).forEach((letter) => {

                    if (index > 0) { // Index 0 contains titles

                        // Check that column has title(something in index 0)
                        if (charArrays[letter][0] && charArrays[letter][0].v != "") {

                            if (charArrays[letter][index]) {

                                item.push(charArrays[letter][index].v);

                            } else {

                                item.push("");

                            }

                        }

                    } else {

                        // Check that title is something
                        if (charArrays[letter][index] && charArrays[letter][index].v) {

                            wbdt.labels.push({
                                "title": charArrays[letter][index].v
                            });

                        }

                    }

                });
                items.push(item);

            });
            wbdt.data = items;


            /*
             * Console.log(longestLength);
             * console.log(longestLetter);
             * console.log(items);
             * console.log(wbdt);
             */


            /*
             * HACK
             * FIXME
             * Since this is wrong in this function we set data like this
             */
            app._data.sheetImportData = wbdt;
            app._data.sheetPreviewOn = true;

            // Disable error throwing from DataTable
            $.fn.dataTable.ext.errMode = "none";

            /*
             * Putting try catch here because sometimes throws
             * error even when everything loaded fine
             */
            try {

                $("#import-preview-list").DataTable({
                    "columns": wbdt.labels,
                    "data": wbdt.data
                });

            } catch (err) {

                console.log(`While importing sheet, an error occured: ${err}`);

            }

        };
        if (rABS) {

            reader.readAsBinaryString(f);

        } else {

            reader.readAsArrayBuffer(f);

        }

    },

    "searchInventoryWithBarCode" () {

        $("#modal-barcode").modal("show");
        barCode(
            $("#barcode-video")[0],
            $("input.form-control.input-sm")[0],
            true
        );

    },

    // While editing item user can read barcode to barcode field
    "editItemBarcode" () {

        $("#modal-barcode").modal("show");
        barCode($("#barcode-video")[0], $("#modal-edit-item-Barcode")[0]);

    },

    // While adding item user can read barcode to barcode field
    "addItemBarcode" () {

        $("#modal-barcode").modal("show");
        barCode($("#barcode-video")[0], $("#modal-add-item-Barcode")[0]);

    },

    "emptyCartAndDecreasItems" () {

        const itemIds = [],
            itemQuantities = [],
            self = this;

        this.cart.forEach((item) => {

            itemIds.push(item.itemData[0]);
            itemQuantities.push(item.quantity);

        });

        axios.get(`${this.apiURL}/decreaseItemQuantity/${JSON.stringify(itemIds)}/${JSON.stringify(itemQuantities)}`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {
                }

            }).
            catch((decreaseItemQauntityErr) => {

                console.log(`decreaseItemQuantity ERROR: ${decreaseItemQauntityErr}`);

            });

        this.cart = [];
        this.billBuyerData = [
            "",
            "",
            ""
        ];
        this.cartBillInfoData[0] = "";
        this.cartBillInfoData[1] = "";
        this.cartBillInfoData[2] = "";
        this.cartBillInfoData[3] = "";
        this.billAdditionalInfo = "";
        this.cartTableAdditionalCellsData = [];
        axios.get(`${this.apiURL}/increaseBillCount`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {
                }

            }).
            catch((increaseBillCountErr) => {

                console.log(`increaseBillCount ERROR: ${increaseBillCountErr}`);

            });
        this.changePage("inventory");

    },

    /*
     * Opens new window where bill can be printed
     */
    "printBill" () {

        const html = $("#bill").html(),
            newWindow = window.open();


        $(newWindow.document.head).html(`<title>${
            this.cartBillInfoData[0].replace(new RegExp("/\./g", "u"), "_")
        }-${
            this.cartBillInfoData[1]
        }-${
            this.cartBillInfoData[2].replace(new RegExp("/\ /g", "u"), "_")
        }-${
            this.billBuyerData.join("-").replace(new RegExp("/\ /g", "u"), "_")
        }</title>` +
                `<style>${
                    billStyle
                }</style>`);

        $(newWindow.document.body).html(`<div id="bill">${html}</div>`);
        newWindow.print();

    },

    "toggleInventoryClickType" () {

        const toggleButton = document.
            getElementsByClassName("inventoryClickTypeToggleButton")[0];
        if (this.inventoryItemClickType == "edit") {

            this.inventoryItemClickType = "addToCart";
            toggleButton.className = toggleButton.
                className.replace("glyphicon-edit", "");
            toggleButton.className += " glyphicon-plus-sign";

        } else {

            this.inventoryItemClickType = "edit";
            toggleButton.className = toggleButton.
                className.replace("glyphicon-plus-sign", "");
            toggleButton.className += " glyphicon-edit";

        }

    },

    "changeInventoryItemClickType" (type) {

        $("#inventory-button-edit").removeClass("btn-primary");
        $("#inventory-button-add-to-cart").removeClass("btn-primary");

        if (type === "edit") {

            $("#inventory-button-edit").addClass("btn-primary");
            // Alert("Voit nyt muokata tuotteita klikkaamalla niitä");

        } else if (type === "addToCart") {

            $("#inventory-button-add-to-cart").addClass("btn-primary");
            // Alert("Voit nyt lisätä tuotteita ostoskoriin klikkaamalla");

        }
        this.inventoryItemClickType = type;

    },

    "inventoryItemClick" (event) {

        if (this.inventoryItemClickType === "edit") {

            $("#modal-loading").modal("show");
            if ($("#inventory-list").DataTable().
                row(event.currentTarget).
                data()) {

                app.showInventory();
                setTimeout(() => {

                    app.itemEditedNow = $("#inventory-list").DataTable().
                        row(event.currentTarget).
                        data();
                    $("#modal-edit-inventory").modal("show");
                    $("#modal-loading").modal("hide");

                }, 1000);

            }
            $("div.dataTables_filter input").focus();

        } else if (this.inventoryItemClickType === "addToCart") {

            this.addToCart(app.inventoryTableElement.
                row(event.currentTarget).
                data());

        }

    },

    "getEmployees" () {

        axios.get(`${this.apiURL}/getEmployees`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {

                    this.employees = responseData;

                }

            }).
            catch((getEmployeesErr) => {

                console.log(`getEmployees ERROR: ${getEmployeesErr}`);

            });

    },

    "emptyCart" () {

        this.cart = [];
        this.cartItemAdditionalCellsData = [];

    },

    "addToCart" (item) {

        const containsObject = function containsObject (obj, list) {

                let itemIndex;
                for (itemIndex = 0; itemIndex < list.length; itemIndex++) {

                    if (list[itemIndex].itemData[0] === obj.itemData[0]) {

                        return true;

                    }

                }
                return false;

            },

            cartObject = {
                "cartData": {
                    "0": 0,
                    "1": 24
                },
                "itemData": item,
                "quantity": 1
            };

        // Does the item has price
        if (!item[2] > 0) {

            alert("Tuotteella ei ole hintaa");
            return;

            // Are there any items in inventory

        } else if (!item[1] > 0) {

            alert("Tuotteita ei ole varastossa");
            return;

        } else if (!containsObject(cartObject, this.cart)) {

            this.cart.push(cartObject);
            this.emphasizeElement("menu-li-1");
            return;

        }
        alert("Tuote on jo ostoskorissa!");
        return;


        const isInCart = new Promise((resolve) => {

            self.cart.forEach((item, index) => {

                if (item.itemData[0] === itemId) {

                    resolve(true);

                }
                if (index === self.cart.length - 1) {

                    resolve(false);

                }

            }),
            self = this;

            if (self.cart.length == 0) {

                resolve(false);

            }

        });

        isInCart.then((isIt) => {

            if (isIt === false) {

                self.inventoryItems.forEach((item) => {

                    if (item[0] === itemId) {

                        if (item[1] > 0) {

                            if (item[2] && item[2] > 0) {

                                const cartItem = {
                                    "cartData": {},
                                    "itemData": item,
                                    "quantity": 1
                                };
                                self.cart.push(cartItem);
                                self.cartItemAdditionalCellsTitles.
                                    forEach((title, index) => {

                                        cartItem.cartData[index] = null;

                                    });
                                self.emphasizeElement("menu-li-1");

                            } else {

                                alert("Tuotteella ei ole hintaa!");

                            }

                        } else {

                            alert("Tuotteita ei ole varastossa!");

                        }

                    }

                });

            } else {

                alert("Tuote on jo ostoskorissa!");

            }

        });

        /*
         * This.inventoryItems.forEach(function(item) {
         * if(item[0] == itemId
         * && $.inArray(item, self.cart) === -1
         * ) {
         * if(item[1] > 0) {
         * self.cart.push(item);
         * self.cartQuantities[itemId] = 1;
         * }
         * }
         * });
         */

    },

    "removeFromCart" (itemId) {

        const self = this;
        this.cart.forEach((item, index) => {

            if (item.itemData[0] === itemId) {

                self.cart.splice(index, 1);

            }

        });
        this.cartCalculations();

    },

    "addEmployee" () {

        const employeeEmail = document.
                getElementById("add-employee-email").value,
            employeePassword = document.
                getElementById("add-employee-password").value,
            employeePassword1 = document.
                getElementById("add-employee-password1").value;

        if (employeePassword === employeePassword1) {

            const self = this;
            $("#modal-loading").modal("show");
            axios.get(`${this.apiURL}/addEmployee/
                ${employeeEmail}/${employeePassword}`).
                then((response) => {

                    const responseData = response.data.data;
                    if (this.noResponseErrors(responseData)) {

                        this.getEmployees();

                    }

                }).
                catch((addEmployeeErr) => {

                    console.log(`addEmployee ERROR: ${addEmployeeErr}`);

                });
            $("#modal-loading").modal("hide");

        } else {

            alert("Salasanat eivät täsmää!");

        }

    },

    "removeEmployee" (employeeEmail) {

        if (confirm(`Haluatko varmasti poistaa käyttäjän: ${employeeEmail}`)) {

            axios.get(`${this.apiURL}/${"removeEmployee/"}${employeeEmail}`).
                then((response) => {

                    const responseData = response.data.data;
                    if (this.noResponseErrors(responseData)) {

                        this.getEmployees();

                    }

                }).
                catch((removeEmployeeErr) => {

                    console.log(`removeEmployee ERROR: ${removeEmployeeErr}`);

                });

        }

    },

    "login" () {

        const email = document.getElementById("login-email").value,
            password = document.getElementById("login-password").value;

        if (!email) {

            alert("Sähköposti ei voi olla tyhjä!");
            return;

        }

        if (!password) {

            alert("Salasana ei voi olla tyhjä!");
            return;

        }
        const self = this; // To fix javascripts shitty "this"

        axios.get(`${this.apiURL}/login/${email}/${password}`).
            then((response) => {

                const responseData = response.data.data;

                console.log(response);
                if (this.noResponseErrors(responseData)) {

                    // Get session token from cookie
                    document.cookie.split(";").forEach((key, index) => {

                        const cookie = key.replace(" ", "").split("=");

                        if (cookie[0] == "hhsSessionToken") {

                            self.sessionToken = cookie[1];
                            self.loggedIn = true;
                            self.changePage("inventory");

                        }

                    });
                    this.email = responseData.email;
                    this.getEmployeeStatus();
                    this.getPrefill();

                }


            }).
            catch((loginErr) => {

                console.log(`login ERROR: ${loginErr}`);

            });

    },

    "logout" () {

        if (confirm("Haluatko varmasti kirjautua ulos?")) {

            clearInterval(this.saveStateInterval);
            localStorage.removeItem("DataTables");
            axios.get(`${this.apiURL}/logout`).
                then((response) => {

                    const responseData = response.data.data;
                    if (this.noResponseErrors(responseData)) {
                    }

                }).
                catch((logouErr) => {

                    console.log(`logout ERROR: ${logoutErr}`);

                });
            document.cookie =
                `hhsSessionToken=; expires=
                    Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
            this.cart = [];
            this.sessionToken = "";
            this.loggedIn = false;
            this.isUserEmployee = "none";
            this.changePage("login");
            // Window.location.reload();

        }

    },

    // WIP
    "register" (email, password) {

        axios.get(`${this.apiURL}/register/${email}/${password}`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {
                }

            }).
            catch((registerErr) => {

                console.log(`register ERROR: ${registerErr}`);

            });
        return "Hello";

    },

    "getLabels" () {

        axios.get(`${this.apiURL}/getLabels`).
            then((response) => {

                const responseData = response.data.data;
                if (this.noResponseErrors(responseData)) {

                    const responseData = response.data.data;
                    this.labels = responseData.labels;
                    this.whoseLabels = responseData.owner;

                }

            }).
            catch((getLabelsErr) => {

                console.log(`getLabels ERROR: ${getLabelsErr}`);

            });

    },

    "showInventory" () {

        axios.get(`${this.apiURL}/showInventory`).
            then((response) => {

                const responseData = response.data.data;
                this.inventoryItems = responseData.items;
                this.inventoryLabels = responseData.labels;
                if (!$.fn.dataTable.isDataTable("#inventory-list")) {

                    inventoryDataTableInit(this);
                    this.inventoryTableElement.draw();

                } else {

                    this.inventoryTableElement.clear();
                    this.inventoryTableElement.clear();
                    this.inventoryTableElement.rows.add(this.inventoryItems);
                    this.inventoryTableElement.draw();

                }


                // Add click event to open editing modal
                $("#inventory-list").off("click");
                $("#inventory-list").on("click", "tr", this.inventoryItemClick);

            }).
            catch((showInventoryErr) => {

                console.log(`showInventory ERROR: ${showInventoryErr}`);

            });

    },

    "editInventoryItem" () {

        const newValues = [];
        this.labels.forEach((key) => {

            if (key !== "none" && key) {

                newValues.push(document.
                    getElementById(`modal-edit-item-${key}`).value);

            }

        });

        const newValuesEscaped = encodeURIComponent(JSON.stringify(newValues));

        axios.get(`${this.apiURL}/editItem/${newValues[0]}/
            ${newValuesEscaped}`).
            then((response) => {

                this.showInventory();

            }).
            catch((editItemErr) => {

                console.log(`editItem ERROR: ${editItemErr}`);

            });

    },

    "addItem" (newValues, updateInventory) {

        if (!newValues ||
            Object.prototype.toString.call(newValues) !== "[object Array]") {

            newValues = [];
            this.labels.forEach((key) => {

                if (key !== "none" && key !== "Id") {

                    newValues.push(document.
                        getElementById(`modal-add-item-${key}`).value);
                    document.getElementById(`modal-add-item-${key}`).value = "";

                }

            });
            updateInventory = true;

        }
        const newValuesEscaped = encodeURIComponent(JSON.stringify(newValues));
        axios.get(`${this.apiURL}/addItem/${newValuesEscaped}/${1}`).
            then((response) => {

                const responseData = response.data.data;
                if (responseData.updateInventory === true) {

                    this.showInventory();

                }
                $("#modal-add-item").modal("hide");

            }).
            catch((addItemErr) => {

                console.log(`addItem ERROR: ${addItemErr}`);

            });

        if (updateInventory === true) {

            this.showInventory();

        }

    },

    "removeInventoryItem" (itemId) {

        if (confirm("Oletko varma että haluat poistaa tuotteen?")) {

            axios.get(`${this.apiURL}/${"removeItem/"}${itemId}`).
                then((response) => {

                    $("#modal-edit-inventory").modal("hide");
                    this.showInventory();

                }).
                catch((removeItemErr) => {

                    console.log(`removeItem ERROR: ${removeItemErr}`);

                });

        }

    },

    "getBillCount" () {

        axios.get(`${this.apiURL}/getBillCount`).
            then((response) => {

                const responseData = response.data.data;
                let billCount = String(responseData.billCount);
                while (billCount.length < 4) {

                    billCount = `0${billCount}`;

                }
                this.cartBillInfoData[1] = billCount;

            }).
            catch((getBillCountErr) => {

                console.log(`getBillCount ERROR: ${getBillCountErr}`);

            });

    },

    "initPageInventory" () {

        this.getLabels();
        this.showInventory();

    },

    "initPageEmployees" () {

        this.getEmployees();

    },

    "initPageCart" () {

        // Update date to this date
        this.cartBillInfoData[0] = moment().format("DD.MM.YYYY");

        // Add or update billCount
        this.getBillCount();

        // Add viitenumero to bill if it doesnt exist
        if (!this.cartBillInfoData[2] || this.cartBillInfoData[2] === "") {

            // Get date, we use this in creating unique number
            const billDate = new Date(),
                r2Base = String(Math.floor(Math.random() * 999));

            let dBase = String(billDate.getDate()) +
                    (billDate.getMonth() + 1) +
                    billDate.getFullYear(),
                r1Base = String(Math.floor(Math.random() * 9999)),
                rBase = String(Math.floor(Math.random() * 9999));

            // Make sure all numbers are 4 in lenght
            while (rBase.length < 4) {

                rBase = `0${rBase}`;

            }

            while (r1Base.length < 4) {

                r1Base = `0${r1Base}`;

            }

            // Add space for human readability
            dBase = dBase.slice(0, 4);
            r1Base = `${r1Base.slice(0, 4)} ${r1Base.slice(4)}`;

            const billNumber = String(`${rBase} ${dBase} ${r1Base}`) + r2Base;
            this.cartBillInfoData[2] = billNumber;

        }

        this.getLabels();
        this.cartCalculations();

    },

    "updatePageFromHash" () {

        this.changePage(window.location.hash.replace("#", ""));

    },

    "changePage" (page) {

        window.location.hash = `#${page}`;
        this.page = page;

        if (page === "inventory") {

            this.initPageInventory();

        } else if (page === "employees") {

            this.initPageEmployees();

        } else if (page === "cart") {

            this.initPageCart();

        }
        document.title = this.pageTitles[page];

    },


    "noResponseErrors": (responseData) => {

        if (responseData.error) {
            console.log(`responseData ERROR: ${JSON.stringify(responseData)}`);

            if (responseData.error == "Et ole kirjautunut sisään") {

                //FIXME app should be this but its not currently working
                app.changePage("login");
                return false;

            }

            alert(responseData.error);
            return false;


        }
        return true;


    }
};
