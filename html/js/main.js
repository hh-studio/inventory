"use strict";

const app = new Vue({

    "data": appData,

    "el": "#app",

    "methods": appMethods,

    "watch": appWatch

});

app.init();
