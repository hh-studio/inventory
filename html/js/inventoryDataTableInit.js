/* globals $, inventoryDataTableInit */

function inventoryDataTableInit(app) {
    var exportOptions = {
        columns: ":visible",
        title: false
    };

    app.inventoryTableElement = $("#inventory-list").DataTable({
        dom: "<\"col-xs-12 col-md-6\"B><\"col-xs-12 col-md-6\"p><\"col-xs-12 col-md-6\"i><\"col-xs-12 col-md-6\"f>rt",
        pagingType: "simple_numbers",
        columnDefs: [{
            targets: [0],
            visible: false,
            searchable: false
        }],
        buttons: [{
                extend: "collection",
                text: "<span class='glyphicon glyphicon-download'></span>",
                className: "col-xs-12 col-md-1",
                fade: true,
                autoClose: true,
                buttons: [{
                        text: "Lataa TSV",
                        extend: "csvHtml5",
                        fieldSeparator: "\t",
                        extension: ".tsv",
                        title: "",
                        exportOptions: exportOptions
                    },
                    {
                        text: "Lataa CSV",
                        extend: "csvHtml5",
                        fieldSeparator: ",",
                        extension: ".csv",
                        title: "",
                        exportOptions: exportOptions
                    },
                    {
                        text: "Lataa PDF",
                        extend: "pdf",
                        title: "",
                        exportOptions: exportOptions
                    },
                    {
                        text: "Lataa Excel",
                        extend: "excel",
                        title: "",
                        exportOptions: exportOptions
                    },
                ]
            },
            {
                text: "<span class='glyphicon glyphicon-stats'></span>",
                className: "col-xs-12 col-md-1",
                action: app.showStats,
            },
            {
                text: "<span class='glyphicon glyphicon-refresh'></span>",
                className: "col-xs-12 col-md-1",
                action: app.showInventory
            },
            {
                text: "<span class='glyphicon glyphicon-plus'></span> Lisää",
                className: "col-xs-12 col-md-2",
                action: function() {
                    $("#modal-add-item").modal("show");
                },
            },
            {
                text: "<span class='glyphicon glyphicon-barcode'></span> Viivakoodi",
                className: "col-xs-12 col-md-3",
                action: function() {
                    app.searchInventoryWithBarCode();
                },
            },
            {
                text: " Toiminto",
                className: "col-xs-12 col-md-3 inventoryClickTypeToggleButton glyphicon glyphicon-plus-sign",
                action: function() {
                    app.toggleInventoryClickType();
                },
            }
            /*
            {
                extend: "collection",
                className: "col-xs-12 col-md-3",
                text: "Toiminto:",
                fade: true,
                autoClose: true,
                buttons: [{
                        text: "<span class='glyphicon glyphicon-edit'></span>Muokkaa",
                        className: "editItemChosen",
                        action: function() {
                            app.inventoryItemClickType = "edit";
                            $(".editItemChosen").addClass = "btn-primary";
                        }
                    },
                    {
                        text: "<span class='glyphicon glyphicon-plus-sign'></span>Lisää ostoskoriin ",
                        className: "addToCartChosen",
                        action: function() {
                            app.inventoryItemClickType = "addToCart";
                        }
                    },
                    {
                        text: "Toiminto tapahtuu kun klikkaa varastotaulukon riviä",
                    }
                ]
            }
            */
        ],
        bStateSave: true,
        scrollX: true,
        fnStateSave: function(oSettings, oData) {
            localStorage.setItem('DataTables', JSON.stringify(oData));
        },
        fnStateLoad: function(oSettings) {
            return JSON.parse(localStorage.getItem('DataTables'));
        },
        data: app.inventoryItems,
        columns: app.inventoryLabels
    });

}
