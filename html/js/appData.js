"use strict";

const appData = {
    "apiURL": "/api",
    "billAdditionalInfo": "",
    "billBuyerData": [
        "",
        "",
        ""
    ],
    "billSellerData": [
        "",
        "",
        "",
        "",
        ""
    ],
    "cart": [],

    "cartBillInfoData": [],

    "cartBillInfoTitles": [
        "Päiväys",
        "Laskun numero",
        "Viitenumero",
        "Eräpäivä",
        "Maksuehto",
        "Viivästyskorko",
        "Pankki",
        "Tilinumero",
        "Swift/BIC"
    ],

    "cartItemAdditionalCellsTitles": [
        "ALE%",
        "ALV%",
        "a hinta sis. ALV",
        "ALE hinta sis. ALV",
        "ALE hinta ALV 0%"
    ],

    "cartTableAdditionalCellsTitles": [
        "Veroton hinta yht.",
        "Veroprosentti",
        "Arvonlisävero yht.",
        "Yhteensä"
    ],

    "cartTableAdditionalCellsData": [
        0,
        0
    ],

    "cartToBill": {
        "0": false,
        "1": false,
        "2": false,
        "3": false,
        "4": false,
        "5": false,
        "6": false,
        "7": false,
        "8": false,
        "9": false,
        "10": false,
        "11": false,
        "12": false,
        "13": false,
        "14": false,
        "15": false,
        "16": false,
        "17": false,
        "18": false,
        "19": false,
        "20": false,
        "21": false,
        "22": false,
        "23": false,
        "24": false,
        "25": false,
        "26": false,
        "27": false,
        "28": false
    },

    "email": "Kirjaudu ulos",

    "employees": [],

    "inventoryItemClickType": "addToCart",
    // List of items shown on inventory page
    "inventoryItems": [],
    // Labels of items shown in inventory page
    "inventoryLabels": [],
    // For to temporarily store the data of item being edited
    "itemEditedNow": [],

    // Items labels
    "labels": [],

    "loadingInfo": "",

    "loggedIn": false,

    "menuTitle": "Varasto - Suomen Autolasitukku",
    // Used to show right page
    "page": "home",
    // Used to show right <title> in browser
    "pageTitles": {
        "cart": "Ostoskori",
        "employees": "Käyttäjät",
        "home": "Etusivu",
        "import": "Tuo taulukko",
        "inventory": "Varasto",
        "login": "Kirjaudu",
        "settings": "Asetukset"
    },
    "sessionToken": "none",
    "sheetImportData": "",
    "sheetPreviewOn": false,
    "stats": [],
    "userIsEmployee": "none",
    // Email of the labels owner, employer or employee
    "whoseLabels": "none"
};
