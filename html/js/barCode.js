function barCode(cameraElement, resultElement, searchAlso) {
    Quagga.init({
        inputStream : {
                name : "Live",
                type : "LiveStream",
                target: cameraElement
            },
            decoder : {
                readers : [
                    "ean_reader",
                    "code_128_reader",
                    "ean_8_reader",
                    "code_39_reader",
                    "code_39_vin_reader",
                    "codabar_reader",
                    "upc_reader",
                    "upc_e_reader",
                    "i2of5_reader",
                    "2of5_reader",
                    "code_93_reader"
                ]
            },
            locate: true,
            debug: {
                drawBoundingBox: true,
                showFrequency: false,
                drawScanline: true,
                showPattern: true
            },
            area: { // defines rectangle of the detection/localization area
                top: "0%",    // top offset
                right: "0%",  // right offset
                left: "0%",   // left offset
                bottom: "0%"  // bottom offset
            }
        }, function(err) {
        if (err) {
            console.log(err);
            return
        }
        console.log("Initialization finished. Ready to start");
        Quagga.start();
    });

    Quagga.onDetected(function(data) {
        Quagga.stop();
        console.log(data);
        resultElement.value = data.codeResult.code;
        if(searchAlso) {
            var table = $("#inventory-list").DataTable();
            table.search(data.codeResult.code).draw();
        }
        $("#modal-barcode").modal("hide");
    });
}
