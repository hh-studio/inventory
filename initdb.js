"use strict";

module.exports = () => {

    global.db.serialize(() => {

        global.db.run(`
            CREATE TABLE if not exists users (
                id INTEGER PRIMARY KEY,
                email TEXT NOT NULL UNIQUE,
                password TEXT NOT NULL,
                session_token TEXT,
                billCount INT
            );
        `);

        global.db.run(`
            CREATE TABLE if not exists labels (
                owner TEXT PRIMARY KEY NOT NULL,
                id TEXT,
                quantity TEXT,
                price TEXT,
                barcode TEXT,
                field1 TEXT,
                field2 TEXT,
                field3 eEXT,
                field4 TEXT,
                field5 TEXT,
                field6 TEXT,
                field7 TEXT,
                field8 TEXT,
                field9 TEXT,
                field10 TEXT,
                field11 TEXT,
                field12 TEXT,
                field13 TEXT,
                field14 TEXT,
                field15 TEXT,
                field16 TEXT,
                field17 TEXT,
                field18 TEXT,
                field19 TEXT,
                field20 TEXT,
                field21 TEXT,
                field22 TEXT,
                field23 TEXT,
                field24 TEXT,
                field25 TEXT,
                FOREIGN KEY(owner) REFERENCES users(email)
            );
        `);

        global.db.run(`
            CREATE TABLE if not exists items (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                owner TEXT NOT NULL,
                quantity INT,
                price INT,
                barcode TEXT,
                field1 TEXT,
                field2 TEXT,
                field3 TEXT,
                field4 TEXT,
                field5 TEXT,
                field6 TEXT,
                field7 TEXT,
                field8 TEXT,
                field9 TEXT,
                field10 TEXT,
                field11 TEXT,
                field12 TEXT,
                field13 TEXT,
                field14 TEXT,
                field15 TEXT,
                field16 TEXT,
                field17 TEXT,
                field18 TEXT,
                field19 TEXT,
                field20 TEXT,
                field21 TEXT,
                field22 TEXT,
                field23 TEXT,
                field24 TEXT,
                field25 TEXT,
                FOREIGN KEY (owner) REFERENCES sers(email)
            );
        `);

        global.db.run(`
            CREATE TABLE if not exists customers (
                id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,
                email TEXT NOT NULL,
                company_name TEXT,
                company_id TEXT,
                customer_id TEXT,
                customer_address TEXT,
                customer_zip_code TEXT,
                customer_name TEXT,
                customer_city TEXT,
                customer_phone_number TEXT,
                FOREIGN KEY (email) REFERENCES users(email)
            );
        `);

        global.db.run(`
            CREATE TABLE if not exists employees (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                employer TEXT NOT NULL,
                employee TEXT NOT NULL UNIQUE,
                FOREIGN KEY (employer) REFERENCES users(email),
                FOREIGN KEY (employee) REFERENCES users(email)
            );
        `);

        global.db.run(`
            CREATE TABLE if not exists prefill (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                owner TEXT,
                seller TEXT,
                cartBillInfoData TEXT,
                FOREIGN KEY (owner) REFERENCES users(email)
            );
        `);

    });

};
